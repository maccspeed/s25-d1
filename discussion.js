db.fruits.insertMany([
{
    "name" : "Apple",
    "color" : "Red",
    "stock" : 20,
    "price" : 40,
    "supplier_id" : 1,
    "onSale" : true,
    "origin" : ["Philippines", "US"]
},
{
    "name" : "Banana",
    "color" : "Yellow",
    "stock" : 15,
    "price" : 20,
    "supplier_id" : 2,
    "onSale" : true,
    "origin" : ["Philippines", "Ecuador"]
},
{
    "name" : "Kiwi",
    "color" : "Green",
    "stock" : 25,
    "price" : 50,
    "supplier_id" : 1,
    "onSale" : true,
    "origin" : ["US", "China"]
},
{
    "name" : "Mango",
    "color" : "Yellow",
    "stock" : 10,
    "price" : 120,
    "supplier_id" : 2,
    "onSale" : false,
    "origin" : ["Philippines", "India"]
}
]);

// MongoDB Aggregation
// used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.

// Using Aggregate method
/* $match
    - used to pass documents that meet the specified condition(s) top the next pipeline stage/aggregation process.
    Syntax: { $match: { field : value } }

   $group
    - used to group elements together and field-value pairs using the data from the grouped elements
    Syntax: { $group: { _id: "value", fieldResult: "valueResult" } }
*/

db.fruits.aggregate([
{ $match: { "onSale": true} },
{ $group: { "_id": "$supplier_id", total: { $sum: "$stock" } } }
]);


// Sorting aggregated results
/* $sort
    - can be used to change the order of aggregated result
    Syntax: { $sort { field: 1/-1 } }
*/

db.fruits.aggregate([
{ $match: { "onSale": true } },
{ $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } } },
{ $sort: { "total": -1 } }
]);


// Aggregating results based on array fields
/* $unwind
    - deconstruct an array field from a collection/field with an array value to output a result for each element
    Syntax: { $unwind: field }
*/
db.fruits.aggregate([
{ $unwind: "$origin" },
{ $group: { "_id": "$origin", "kinds": { $sum: 1 } } }
]);


// Schema Design

// One-to-One Relationship
var owner = ObjectId();

db.owners.insert({
    "_id": owner,
    "name": "John Smith",
    "contact": "09876543210"
});

db.suppliers.insert({
    "name": "ABC Fruits",
    "contact": "09998876543",
    "owner_id": ObjectId("61777048a736fdf5327a6d2b")
});

// Update the owner document and insert the new field
db.owners.updateOne(
    { "_id": ObjectId("61777048a736fdf5327a6d2b") },
    {
        $set: {
            "supplier_id": ObjectId("61777358a736fdf5327a6d2f")
        }
    }
);


// One-to-Many Relationship
db.suppliers.insertMany([
{
    "name": "DEF Fruits",
    "contact": "09632109847",
    "owner_id": ObjectId("61777048a736fdf5327a6d2b")
},
{
    "name": "GHI Fruits",
    "contact": "09065489712",
    "owner_id": ObjectId("61777048a736fdf5327a6d2b")
}
]);

db.owners.updateOne(
{ "_id": ObjectId("61777048a736fdf5327a6d2b") },
{
    $set: {
        "supplier_id":
        [
            ObjectId("61777358a736fdf5327a6d2f"),
            ObjectId("617779faa736fdf5327a6d30"),
            ObjectId("617779faa736fdf5327a6d31")
        ]
    }
}
);